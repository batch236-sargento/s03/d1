package com.zuitt.example;

import java.util.Scanner;

public class S3A1 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int num = 1;
        int a = 1;

        System.out.println("Input an integer whose factorial will be computed: ");

        try {
            num = input.nextInt();
            if (num > 0) {
                int i = num;
                while (i >= 1) {
                    a = i * a;
                    i--;
                }
            } else {
                num = 1;
                System.out.println("Invalid input!");
            }
        }
        catch (Exception e) {
            System.out.println("Invalid Input!");
            e.printStackTrace();
        }
        finally {
            System.out.println("The factorial of " + num + " is " + a);
        }

    }
}

